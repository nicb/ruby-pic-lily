# The `barebones` example

The `barebones` example aims to do the first and more basic proof of concept.
Is it possible to:

1. create an `encapsulated postscript` example of a lilypond fragment

1. put it inside a `pic` box anywhere on the page

Things to check:

1. is it at all possible to do it?

1. how do the `box` size and the fragment size relate?

1. will the fragment follow the box anywhere on the page?
