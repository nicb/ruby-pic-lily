# `ruby-pic-lily`: proof of concept

This folder contains some tests which combine together `pic` with `lilypond`,
just to check what needs to be done for both programs to interoperate. If you
want to provide examples, please create a new directory under the folder
`examples` and provide an explanation of what you have done.

