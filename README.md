# Ruby + Pic + Lilypond

**PLEASE NOTE: this project is in an embryonal stage. If you are interested in
contributing, please follow the rules below.**

This project aims at putting together the functionalities provided by the [`pic`](http://git.savannah.gnu.org/cgit/groff.git/tree/src/preproc/pic)
(a preprocessor of the [`groff`](http://git.savannah.gnu.org/cgit/groff.git)
typesetting system) and the [`lilypond`](http://lilypond.org/source.html) languages
in order to create an adequate graphic tool for contemporary music writing and
typesetting.

This repository includes [a proof-of-concept folder](./poc/README.md) and [a `ruby` gem](./RPL/README.md).

## Requirements

The software contained herein works in close conjunction with 

* the `groff` suite (which incorporates `pic` and provides the drivers to `ps/pdf`)
* the `lilypond` software (http://lilypond.org/source.html) 

## Contributing

Bug reports and pull requests are welcome on smerm git at https://git.smerm.org/nicb/ruby-pic-lily

## License

All sources contained herein are licensed with the [GNU GPL license v3](./LICENSE).
